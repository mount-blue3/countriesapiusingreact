import React from "react";
import { BiSearch } from "react-icons/bi";

function Filter({ onFilter, filter, currencies }) {
  const handleSearchChange = (event) => {
    onFilter({ ...filter, searchText: event.target.value });
  };

  const handleRegionChange = (event) => {
    onFilter({ ...filter, region: event.target.value });
  };
  const handleCurrencyChange = (event) => {
    onFilter({ ...filter, currency: event.target.value });
  };

  return (
    <div className="filter-container">
      <div className="search-input">
        <BiSearch className="search-icon" />
        <input
          type="search"
          value={filter.searchText}
          placeholder="Search for a country..."
          onChange={handleSearchChange}
        />
      </div>
      <div>
        <select
          className="region-container"
          value={filter.region}
          onChange={handleRegionChange}
        >
          <option className="option" value="">
            Filter by Region
          </option>
          <option className="option" value="africa">
            Africa
          </option>
          <option className="option" value="americas">
            Americas
          </option>
          <option className="option" value="asia">
            Asia
          </option>
          <option className="option" value="europe">
            Europe
          </option>
          <option className="option" value="oceania">
            Oceania
          </option>
        </select>
      </div>
      <div>
        <select
          className="currency-container"
          value={filter.currency}
          onChange={handleCurrencyChange}
        >
          <option className="option" value="">
            All
          </option>
          {currencies.map((currency) => (
            <option key={currency} className="option" value={currency}>
              {currency}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
}

export default Filter;
