import React, { useEffect, useState, createContext } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Countries from "./Countries";
import Country from "./Country";
import "./App.css";
import { FaMoon } from "react-icons/fa";
import axios from "axios";
import Filter from "./Filter";

export const ModeContext = createContext(null);

function App() {
  const [mode, setMode] = useState("light");
  const toggleMode = () =>
    setMode((curr) => (curr === "light" ? "dark" : "light"));

  const [countries, setCountries] = useState([]);
  const [filter, setFilter] = useState({
    searchText: "",
    region: "",
    currency: "",
  });
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    getCountries();
  }, []);

  const getCountries = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get("https://restcountries.com/v3.1/all");
      const countriesData = response.data;
      setCountries(countriesData);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
      console.log(error);
    }
  };

  const handleFilter = ({ searchText, region, currency }) => {
    setFilter({
      searchText: searchText.toLowerCase(),
      region: region.toLowerCase(),
      currency: currency,
    });
  };
  const currencies = () => {
    let currencyNames = [];
    countries.forEach((item) => {
      const keys = item.currencies && Object.values(item.currencies);
      if (keys !== undefined) {
        const names = keys.map((currency) => currency.name);
        currencyNames = currencyNames.concat(names);
      }
    });
    return currencyNames;
  };

  const filterCountries = () => {
    const { searchText, region, currency } = filter;

    let filteredCountries = countries;

    if (searchText) {
      filteredCountries = filteredCountries.filter((item) =>
        item.name.common.toLowerCase().includes(searchText)
      );
    }

    if (region) {
      filteredCountries = filteredCountries.filter((item) =>
        item.region.toLowerCase().includes(region)
      );
    }

    if (currency) {
      filteredCountries = filteredCountries.filter((item) =>
        Object.values(item.currencies || {}).some((curr) =>
          curr.name.includes(currency)
        )
      );
    }

    return filteredCountries;
  };

  return (
    <ModeContext.Provider value={{ mode, toggleMode }}>
      <div className="container" id={mode}>
        <Router>
          <div className="header">
            <h2>Where in the world?</h2>
            <div className="mode-toggle">
              <FaMoon className="moon-icon" />
              <button className="mode-button" onClick={toggleMode}>
                {mode}
              </button>
            </div>
          </div>
          <div className="content">
            {isError && (
              <div className="error">
                Error while loading countries. Please try again.
              </div>
            )}
            {isLoading && <div className="loading">Loading...</div>}
            {!isError && !isLoading && (
              <Routes>
                <Route
                  path="/"
                  element={
                    <div className="country-card">
                      <Filter
                        onFilter={handleFilter}
                        filter={filter}
                        currencies={currencies()}
                      />
                      <Countries data={filterCountries()} />
                    </div>
                  }
                />
                <Route path="/countries/:countryName" element={<Country />} />
              </Routes>
            )}
          </div>
        </Router>
      </div>
    </ModeContext.Provider>
  );
}

export default App;
