import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useNavigate, useParams } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";

function Country() {
  const [countryData, setCountryData] = useState([]);
  const [borderNames, setBorderNames] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const navigate = useNavigate();
  const { countryName } = useParams();
  const getCountryData = async (countryName) => {
    try {
      setIsLoading(true);
      const response = await axios.get(
        `https://restcountries.com/v3.1/name/${countryName}`
      );
      const data = response.data;
      setCountryData(data);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
      console.log(error);
    }
  };
  const getCountryNames = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get("https://restcountries.com/v3.1/all");
      const countries = response.data;
      const mapping = {};
      countries.forEach((country) => {
        mapping[country.cca3] = country.name.common;
      });
      setBorderNames(mapping);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
      console.log(error);
    }
  };

  useEffect(() => {
    getCountryData(countryName);
    getCountryNames();
  }, [countryName]);

  const handleClick = () => {
    navigate("/");
  };
  if (!countryData.length) {
    return null;
  }
  console.log(countryData, "countryData");

  const {
    flags,
    name,
    population,
    region,
    subregion,
    capital,
    tld,
    currencies,
    languages,
    borders,
  } = countryData[0];
  const nativeLanguages = languages
    ? Object.values(languages)
    : ["no native languages"];
  const nativeName = Object.values(name)[2]
    ? Object.values(Object.values(name)[2])[0].common
    : ["no native name"];
  const currency = currencies
    ? Object.values(Object.values(currencies)[0])[0]
    : ["no currency"];

  return (
    <div className="country-detail-info">
      {isError && (
        <div className="error">
          Error while loading country data. Please try again.
        </div>
      )}
      {isLoading && <div className="loading">Loading...</div>}
      {!isError && !isLoading && (
        <>
          <div className="back-button-container">
            <FiArrowLeft className="arrow-icon" />
            <button className="back-button" onClick={handleClick}>
              Back
            </button>
          </div>
          <div className="single-country-data">
            <div className="flag-container">
              <img src={flags.png} alt="country" />
            </div>
            <div className="single-country-details1">
              <h3>
                Native Name: <span>{nativeName}</span>
              </h3>
              <h3>
                Population: <span>{population}</span>
              </h3>
              <h3>
                Region: <span>{region}</span>
              </h3>
              <h3>
                Sub Region: <span>{subregion}</span>
              </h3>
              <h3>
                Capital: <span>{capital ? capital[0] : name.common}</span>
              </h3>
            </div>
            <div className="single-country-details2">
              <h3>
                Top Level Domain: <span>{tld[0]}</span>
              </h3>
              <h3>
                Currencies: <span>{currency}</span>
              </h3>
              <h3>
                Languages: <span>{nativeLanguages.join(", ")}</span>
              </h3>
            </div>
          </div>
          {borders && borders.length > 0 && (
            <div className="borders">
              <h3>
                Border countries:
                <span>
                  {borders.map((item, index) => {
                    const completeName = borderNames[item] || item;
                    return (
                      <Link key={index} to={`/countries/${completeName}`}>
                        <button>{completeName}</button>
                      </Link>
                    );
                  })}
                </span>
              </h3>
            </div>
          )}
        </>
      )}
    </div>
  );
}

export default Country;
