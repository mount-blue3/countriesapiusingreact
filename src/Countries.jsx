import React from "react";
import { Link } from "react-router-dom";

function Countries({ data }) {
  return (
    <>
      {data.length !== 0 ? (
        data.map((item, index) => {
          const { name, population, region, flags } = item;
          let capitalName =
            item.capital === undefined ? name.common : item.capital[0];

          return (
            <Link
              to={`/countries/${name.common}`}
              key={index}
              className="country-link"
            >
              <div className="country">
                <img src={flags.png} alt={name.common} />
                <div className="country-info">
                  <h2 className="name">{name.common}</h2>
                  <h4>
                    Population: <span>{population}</span>
                  </h4>
                  <h4>
                    Region: <span>{region}</span>
                  </h4>
                  <h4>
                    Capital: <span>{capitalName}</span>
                  </h4>
                </div>
              </div>
            </Link>
          );
        })
      ) : (
        <h1>No countries found</h1>
      )}
    </>
  );
}

export default Countries;
